package arraysParametros;

public class Ej2 {

	public static int mayor(int[][] a){
		
		int mayor=a[0][0];

		for(int i=0;i<a[0].length;i++)
			for(int j=1;j<a[1].length-1;j++)
				if(a[i][j]>mayor)
					mayor = a[i][j];	
		return mayor;
		
	}
	
	public static void main(String[] args) {
		
		int a[][] = {{1,2,3},{4,5,6}};
		System.out.println("Mayor: "+mayor(a));

	}

}
