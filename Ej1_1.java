package arraysParametros;

import java.util.Arrays;
import java.util.Scanner;

public class Ej1_1 {
	
	public static int mayor(int[] a){
		int mayor=a[0];
		for(int i=1;i<a.length;i++)
			if(a[i]>mayor)
				mayor = a[i];	
		return mayor;
	}
	
	public static int menor(int[] a){
		int menor=a[0];
		for(int i=1;i<a.length;i++)
			if(a[i]<menor)
				menor = a[i];	
		return menor;
	}
	
	public static void generarValores(int[] a){
		for(int i=0;i<a.length;i++)
			a[i]=(int)(Math.random()*9)+1;
	}
	
	public static void introducirValores(int[] a){
		Scanner sc = new Scanner(System.in);
		boolean salir = false;
		
		for(int i=0;i<a.length;i++){
			do{
				System.out.print("Introduce un numero entero: ");
				try{
					a[i] = sc.nextInt();
					salir = false;
				}catch(Exception e){
					salir = true;
				}finally{
					sc.nextLine();
				}
			}while(salir);
		}		
		sc.close();
	}
	
	public static void main(String[] args) {
		
		int a[] = {30,1000,200,500,10};
		int b[] = new int[5];
		int c[] = new int[5];	
		
		generarValores(b);
		introducirValores(c);
		
		System.out.println("Mayor: "+mayor(a)+"\tMenor: "+menor(a)+"\tArray: "+Arrays.toString(a));
		System.out.println("Mayor: "+mayor(b)+"\tMenor: "+menor(b)+"\tArray: "+Arrays.toString(b));
		System.out.println("Mayor: "+mayor(c)+"\tMenor: "+menor(c)+"\tArray: "+Arrays.toString(c));
		
	}
	
}