package arraysParametros;

public class Ej1 {

	public static int sum(int[] a){
		int sum=0;
		for(int i=0;i<a.length;i++)
			sum+=a[i];
		return sum;
	}
	
	public static void main(String[] args) {
		
		int a[] = {3,1,2,5,4};
		System.out.println(sum(a));

	}

}